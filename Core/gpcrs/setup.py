import requests

host = 'http://localhost:5000'
id_param = '?id=e15879db9d6b43649e844bea6b47b8c1'

car = '/car'
cars = '/cars'
fingerprint = '/fingerprint'

to_create = [car, fingerprint]

for i in range(1, 100):
    for item in to_create:
        ret = requests.post(host + item + id_param)
        print(f'made {item} status {ret}')