DROP TABLE IF EXISTS cars;
DROP TABLE IF EXISTS confirmed_reservations;
DROP TABLE IF EXISTS fingerprints;

CREATE TABLE cars (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    status TEXT,
    fingerprint TEXT ,
    last_updated TIMESTAMP,
    expires TIMESTAMP
);

CREATE TABLE fingerprints (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    fingerprint TEXT NOT NULL,
    admin INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE confirmed_reservations (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    car_id INTEGER NOT NULL,
    parent TEXT NOT NULL,
    child TEXT NOT NULL,
    email TEXT NOT NULL,
    grade TEXT NOT NULL,
    club TEXT NOT NULL,
    reserved_at DATETIME NOT_NULL,
    FOREIGN KEY (car_id) REFERENCES car (id)
);