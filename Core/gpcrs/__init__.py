import os
import json
import datetime
import uuid
from openpyxl import Workbook

from flask import Flask, request, Response, send_from_directory

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'gpcrs.sqlite'), 
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # laod the test config if passed in
        app.config.from_mapping(test_config)

    # ensure that the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .db import init_app, get_db
    init_app(app)

    @app.route('/')
    def base():
        return send_from_directory('../../Client/public', 'index.html')

    @app.route('/<path:path>')
    def home(path):
        return send_from_directory('../../Client/public', path)

    def res_factory(row):
        row_list = list(row)
        for idx, item in enumerate(row):
            if type(item) == datetime.datetime:
                row_list[idx] = item.isoformat()
            elif item == None:
                row_list[idx] = 'null'
        res = {
            'carId': row_list[1],
            'parent': row_list[2],
            'child': row_list[3],
            'email': row_list[4],
            'grade': row_list[5],
            'club': row_list[6],
        }

        return (res)

    def car_factory(row):
        row_list = list(row)
        for idx, item in enumerate(row):
            if type(item) == datetime.datetime:
                row_list[idx] = item.isoformat()
            elif item == None:
                row_list[idx] = 'null'
        car = {
            'id': row_list[0],
            'status': row_list[1],
            'fingerprint': row_list[2],
            'last_updated': row_list[3],
            'expires': row_list[4]
        }

        return (car)
        
    def fingerprint_factory(row):
        row_list = list(row)
        for idx, item in enumerate(row):
            if type(item) == datetime.datetime:
                row_list[idx] = item.isoformat()
            elif item == None:
                row_list[idx] = 'null'
        fingerprint = {
            'id': row_list[0],
            'fingerprint': row_list[1],
            'admin': row_list[2]
        }

        return(fingerprint)

    def get_car(car_id):
        db = get_db()
        row = db.execute(
            'SELECT c.*'
            ' FROM cars c'
            ' WHERE c.id = ?',
            [int(car_id)]
        ).fetchone()

        car = car_factory(row)

        return car

    def make_car():
        db = get_db()
        db.execute(
            'INSERT INTO cars'
            ' (status, last_updated)'
            ' VALUES (?,datetime("now"))',
            ['available']
        )
        ret = db.commit()
        return 'ok'

    @app.route('/api/fingerprint', methods=['POST'])
    def make_fingerprint():
        if not request.args.get('id'):
            fingerprint = uuid.uuid4().hex[3:8].upper()
        else: 
            fingerprint = request.args.get('id')
        db = get_db()
        db.execute(
            'INSERT INTO fingerprints'
            ' (fingerprint)'
            ' VALUES (?)',
            [fingerprint]
        )
        db.commit()
        return f'created fingerprint {fingerprint}'

    @app.route('/api/login', methods=['PUT'])
    def login():
        fingerprint = request.args.get('id')
        db = get_db()
        row = db.execute(
            'SELECT * FROM fingerprints'
            ' WHERE fingerprint = ?',
            [fingerprint]
        ).fetchone()

        if row is not None:
            row = dict(row)
            fingerprint = row['fingerprint']
            admin = row['admin']
            response = {'message': 'ok', 'data': {'fingerprint': fingerprint, 'admin': admin}}
            return Response(json.dumps(response), 200)
        else:
            return Response(json.dumps({'message': '401 unauthorized'}), 401)

    def authenticate_fingerprint(fingerprint):
        db = get_db()
        row = db.execute(
            'SELECT fingerprint FROM fingerprints'
            ' WHERE fingerprint = ?',
            [fingerprint]
        ).fetchone()
        ret = row
        return ret is not None

    @app.route('/api/fingerprints')
    def get_fingerprints():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        admin = is_admin(fingerprint)
        if not authenticated or not admin:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} unauthorized', 401)
        db = get_db()
        rows = db.execute(
            'SELECT f.*'
            ' FROM fingerprints f'
        ).fetchall()
        fingerprints = []
        [fingerprints.append(fingerprint_factory(fingerprint)) for fingerprint in rows]
        return json.dumps(fingerprints)

    @app.route('/api/clear-cars', methods=['PUT'])
    def clear_cars():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        if not authenticated:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} not found', 401)
        db = get_db()
        db.execute(
            'UPDATE cars '
            ' SET status = "available",'
            '   fingerprint = ?,'
            '   last_updated = datetime("now"),'
            '   expires = datetime("1970-01-01 00:00:00")'
            ' WHERE fingerprint = ?'
            ' AND status != "reserved"',
            [fingerprint, fingerprint]
        )
        db.commit()
        return (json.dumps({'message': 'ok'}))

    @app.route('/api/reservation')
    def get_reservation():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        if not authenticated:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} not found', 401)
        db = get_db()
        reservation_row = db.execute(
            'SELECT r.*'
            ' FROM confirmed_reservations r'
            ' LEFT JOIN cars c on c.id = r.car_id'
            ' WHERE c.fingerprint = ?',
            [fingerprint]
        ).fetchone()
        if reservation_row:
            reservation = dict(reservation_row)
        else:
            reservation = {}
        return json.dumps(reservation)

    @app.route('/api/admin/cars')
    def get_all_cars():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        admin = is_admin(fingerprint)
        if not authenticated or not admin:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} unauthorized', 401)
        db = get_db()
        car_rows = db.execute(
            'SELECT c.*'
            ' FROM cars c'
        ).fetchall()
        car_list = []
        [car_list.append(car_factory(car)) for car in car_rows]
        return json.dumps(car_list)

    def is_admin(fingerprint):
        db = get_db()
        row = db.execute(
            'SELECT *'
            ' FROM fingerprints'
            ' WHERE fingerprint = ?'
            ' AND admin = 1',
            [fingerprint]
        ).fetchall()
        if row is not None:
            row = list(row)
            if len(row) > 0:
                return True
            else:
                return False

    @app.route('/api/admin/reservations')
    def get_reservations():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        admin = is_admin(fingerprint)
        if not authenticated or not admin:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} unauthorized', 401)
        db = get_db()
        reservation_rows = db.execute(
            'SELECT r.*'
            ' FROM confirmed_reservations r'
            ' LEFT JOIN cars c on c.id = r.car_id'
        ).fetchall()
        reservation_list = []
        [reservation_list.append(res_factory(res)) for res in reservation_rows]
        filetype = request.args.get('filetype')
        if filetype is not None:
            if filetype == 'xlsx':
                wb = Workbook()
                ws = wb.active
                ws.append(['Car', 'Parent', 'Clubber', 'Email', 'Grade', 'Club'])
                filename = f'{uuid.uuid4().hex[3:8].upper()}.xlsx'
                for reservation in reservation_list:
                    ws.append([reservation['carId'], reservation['parent'], reservation['child'], reservation['email'], reservation['grade'], reservation['club']])
                wb.save(f'/tmp/{filename}')
                return send_from_directory(
                    '/tmp',
                    filename)
        else:
            return json.dumps(reservation_list)

    @app.route('/api/cars')
    def get_cars():
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        if not authenticated:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} not found', 401)
        db = get_db()
        car_rows = db.execute(
            'SELECT c.*'
            ' FROM cars c'
            ' WHERE (c.status == "available")'
            ' OR'
            ' (c.expires <= datetime("now"))'
        ).fetchall()
        car_list = []
        [car_list.append(car_factory(car)) for car in car_rows]
        return json.dumps(car_list)

    def hold_car(car_id, fingerprint):
        db = get_db()
        db.execute(
            'UPDATE cars'
            ' SET status = ?,'
            '   fingerprint = ?,'
            '   last_updated = datetime("now"),'
            '   expires = datetime("now", "+7 minutes")'
            ' WHERE id = ?',
            ['held', fingerprint, car_id]
        )
        db.commit()
        return

    def finalize_reservation(car_id, fingerprint, data):
        admin = is_admin(fingerprint)
        if not admin:
            db = get_db()
            db.execute(
                'UPDATE cars'
                ' SET status = "reserved",'
                '   fingerprint = ?,'
                '   last_updated = datetime("now"),'
                '   expires = NULL'
                ' WHERE id = ?',
                [fingerprint, car_id]
            )
            db.commit()
            # TODO add empty reservation input rejection
            ret = db.execute(
                'INSERT INTO confirmed_reservations'
                ' (car_id, parent, child, email, grade, club, reserved_at)'
                ' VALUES (?,?,?,?,?,?,datetime("now"))',
                [car_id, data['parent'], data['child'], data['email'], data['grade'], data['club']]
            )
            db.commit()
        return

    def reserve_car(car_id, fingerprint, action, data=None):            
        authenticated = authenticate_fingerprint(fingerprint)
        if not authenticated:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} not found', 401)

        if action == 'hold':
            print(f'holding {fingerprint} reservation for car {car_id}')
            result = hold_car(car_id, fingerprint)
        elif action == 'finalize':
            print(f'finalizing {fingerprint} reservation for car {car_id}')
            finalize_reservation(car_id, fingerprint, data)
        else:
            return Response(f'action {action} unknown', 400)
        
        return Response(json.dumps({'message': 'ok'}), 200)
        

    # TODO move this to /api/
    @app.route('/api/car', defaults={'car_id': None}, methods=['POST'])
    @app.route('/api/car/<int:car_id>', methods=['GET', 'PUT'])
    def car(car_id):
        fingerprint = request.args.get('id')
        authenticated = authenticate_fingerprint(fingerprint)
        if not authenticated:
            fingerprint = 'null' if fingerprint is None else fingerprint
            return Response(f'id {fingerprint} not found', 401)
        if car_id is not None:
            if request.method == 'GET':
                return get_car(car_id)
            elif request.method == 'PUT':
                if request.json:
                    return reserve_car(car_id, fingerprint, request.args.get('action'), request.json)
                else:
                    return reserve_car(car_id, fingerprint, request.args.get('action'))
        else:
            return make_car()

    return app

if __name__ == "__main__":
    app = create_app()
    app.run(host='0.0.0.0')