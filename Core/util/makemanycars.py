import requests as req
import sys

def main():
    hostname = sys.argv[1]
    fingerprint = sys.argv[2]
    count = int(sys.argv[3])

    for i in range(0, count):
        ret = req.post(f'{hostname}/api/car?id={fingerprint}')
        if ret.status_code == 200:
            print(ret.text)
        else:
            print(f'HTTP error {ret.status}: ret.text')
            sys.exit(1)

if __name__ == "__main__":
    required_args = 3
    arg_count = len(sys.argv) - 1
    if arg_count < required_args:
        print(f'{arg_count} args supplied; {required_args} required')
        sys.exit(1)
    main()