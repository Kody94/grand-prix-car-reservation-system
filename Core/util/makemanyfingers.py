import requests as req
import sys
import json

def main():
    hostname = sys.argv[1]
    clubbersfile = sys.argv[2]
    clubbers = []
    with open(clubbersfile, 'r') as f:
        clubbers = json.loads(f.read())
    for clubber in clubbers:
        ret = req.post(f'{hostname}/api/fingerprint?id={clubber}')
        if ret.status_code == 200:
            print(ret.text)
        else:
            print(f'HTTP error {ret.status}: ret.text')
            sys.exit(1)

if __name__ == "__main__":
    required_args = 2
    arg_count = len(sys.argv) - 1
    if arg_count < required_args:
        print(f'{arg_count} args supplied; {required_args} required')
        sys.exit(1)
    main()