import requests as req
import sys

def main():
    hostname = sys.argv[1]
    ret = req.post(f'{hostname}/api/fingerprint')
    if ret.status_code == 200:
        print(ret.text)
    else:
        print(f'HTTP error {ret.status}: ret.text')
        sys.exit(1)

if __name__ == "__main__":
    required_args = 1
    arg_count = len(sys.argv) - 1
    if arg_count < required_args:
        print(f'{arg_count} args supplied; {required_args} required')
        sys.exit(1)
    main()