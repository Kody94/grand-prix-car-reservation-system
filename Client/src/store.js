import { writable } from 'svelte/store';
export const auth = writable({
    'authenticated': false,
    'fingerprint': ''
});